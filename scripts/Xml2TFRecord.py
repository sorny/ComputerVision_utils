import tensorflow as tf
import utils.LabelConverter as LabelConverter
import glob
import sys
sys.path.insert(0, '../models/research/')
from object_detection.utils import dataset_util
import xml.etree.ElementTree as ET
from progress.bar import Bar 


def create_tf_from_xml(example):
  tree = ET.parse(example)
  root = tree.getroot()
  height = int(root.find('size').find('height').text)
  width = int(root.find('size').find('width').text)
  filename = root.find('filename').text.encode()

  with tf.gfile.GFile(LabelConverter.get_image_path_from_label_path(example), 'rb') as fid:
    encoded_image_data = fid.read()
  image_format = b'jpeg' #or b'png'

  xmins = [] # List of normalized left x coordinates in bounding box (1 per box)
  xmaxs = [] # List of normalized right x coordinates in bounding box
             # (1 per box)
  ymins = [] # List of normalized top y coordinates in bounding box (1 per box)
  ymaxs = [] # List of normalized bottom y coordinates in bounding box
             # (1 per box)
  classes_text = [] # List of string class name of bounding box (1 per box)
  classes = [] # List of integer class id of bounding box (1 per box)

  for obj in root.iter('object'):
    cls = obj.find('name').text
    bbox = obj.find('bndbox')
    xmin = float(bbox.find('xmin').text)/ width
    ymin = float(bbox.find('ymin').text)/ height
    xmax = float(bbox.find('xmax').text)/ width
    ymax = float(bbox.find('ymax').text)/ height
    xmins.append(xmin)
    ymins.append(ymin)
    xmaxs.append(xmax)
    ymaxs.append(ymax)
    classes_text.append(cls.encode())
    classes.append(1)

  tf_example = tf.train.Example(features=tf.train.Features(feature={
      'image/height': dataset_util.int64_feature(height),
      'image/width': dataset_util.int64_feature(width),
      'image/filename': dataset_util.bytes_feature(filename),
      'image/source_id': dataset_util.bytes_feature(filename),
      'image/encoded': dataset_util.bytes_feature(encoded_image_data),
      'image/format': dataset_util.bytes_feature(image_format),
      'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
      'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
      'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
      'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
      'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
      'image/object/class/label': dataset_util.int64_list_feature(classes),
  }))
  return tf_example


def main(_):
  writer = tf.python_io.TFRecordWriter('train.tf')

  root ='/media/esteve/1615F2A532ED483C/Ubuntu/ML/fish_dataset/imagenet_dataset/'
  input_train_folders = ['/media/esteve/1615F2A532ED483C/Ubuntu/ML/fish_dataset/imagenet_dataset/imagenet_split_renamed/train.txt']
  for input_train_folder in input_train_folders:
    print(input_train_folder)
    labels_paths = open(input_train_folder, 'r').read().split('\n')
    #labels_paths = glob.glob('{}/labels_voc/*'.format(input_train_folder))
    bar = Bar('Processing input folders', max=len(labels_paths))
    for example in labels_paths[:]:
      label_path= example.split(' ')[1]
      #print(label_path)
      tf_example = create_tf_from_xml(root + label_path)
      writer.write(tf_example.SerializeToString())
      bar.next()
    bar.finish()

  writer.close()


if __name__ == '__main__':
  tf.app.run()